#include "baidu_map.h"

void baidu_map::baidu_map_init(void)
{

    manager = new QNetworkAccessManager();
    post_timer = new QTimer;
    connect(post_timer, SIGNAL(timeout()), this, SLOT(post_slot()));
    /*post 返回信息*/
    connect(manager, SIGNAL(finished(QNetworkReply*)),this,SLOT(replyFinished(QNetworkReply*)));
    //TODO: auto change time
    post_timer->start(1000);
}


/*POST 返回信息接收*/
void baidu_map::replyFinished(QNetworkReply *reply)
{
    QByteArray byte_data;
    //QTextCodec *codec = QTextCodec::codecForName("utf8");
    //QString all = codec->toUnicode(reply->readAll());

     byte_data= reply->readAll();

     prase_ip_info(byte_data);

     post_ack = post_list.at(0);
     post_list.removeFirst();
     emit ip_to_addr_sig();
}

void baidu_map::ip_to_addr(post_ip_info info)
{
    post_list.append(info);
}
void baidu_map::post_slot()
{
    if(post_list.size() == 0)
        return;

    post_ip(post_list.at(0).ip);

}
void baidu_map::post_ip(QString ip)
{

    QString token_str;


    request->setUrl(QUrl("http://api.map.baidu.com/location/ip"));

    QByteArray postData;

    token_str = "ip="+ip;
    token_str +="&ak=yvR4WBKXzCO8K9yN9TdgdYGHMsKBo94i";

    postData.append(token_str);

    manager->post(*request, postData);

    //free(request);

}

void baidu_map::prase_ip_info(QByteArray data)
{

    QJsonParseError jsonError;
    QJsonDocument doucment = QJsonDocument::fromJson(data, &jsonError);  // 转化为 JSON 文档

    if (!doucment.isNull() && (jsonError.error == QJsonParseError::NoError)) {  // 解析未发生错误
        if (doucment.isObject()) {  // JSON 文档为对象
            QJsonObject object = doucment.object();  // 转化为对象
            if (object.contains("address")) {
                QJsonValue value = object.value("address");
                if (value.isString()) {
                    QString strName = value.toString();
                    qDebug() << "address : " << strName;
                    post_list[0].addr = strName;
                }
            }

        }

    }
}

