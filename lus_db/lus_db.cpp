#include "lus_db.h"
#include "lus_data_type.h"


#include <QDebug>
#include <QMessageBox>
#include <QSqlError>
#include <QSqlQuery>
#include <QDateTime>
#include <QFileInfo>

lus_db::lus_db()
{ 
}

//1.新建一个类 lus_database
//2.每个LUS_TY都建立一个数据库tab
//数据库文件是否存在？
//不存在则建立、初始化页表、字段等


/*创建数据库*/
bool lus_db::open_database(void)
{
    if(QSqlDatabase::contains("qt_sql_default_connection"))
        lus_tydb = QSqlDatabase::database("qt_sql_default_connection");
    else
        lus_tydb = QSqlDatabase::addDatabase("QSQLITE");
    lus_tydb.setDatabaseName("lus_tydb.db");//设置数据库文件名字
    if(!lus_tydb.open())
    {
        qDebug() << "打开数据库失败";
    }
    return true;
}

//*初始化数据库
// *
// * */
bool lus_db::lus_database_init(void)
{
    QFileInfo f_inf("lus_tydb.db");
    if(!f_inf.exists()){

        open_database();
        QSqlQuery query;

        //创建一个LUS_UPTIME
        bool success1 = query.exec("create table LUS_UPTIME(current_date_time varchar primary key,fix_id varchar,up_date varchar,"
                                   "down_date varchar)");//创建数据表
        if(success1) {
            qDebug() <<"数据库表1创建成功!";
        } else {
            qDebug() <<"数据库表1创建失败!";
            QSqlError tempErr = query.lastError();
            qDebug()<<tempErr;
            return false;
        }

        //创建一个LUS_TY_COMMENT
        bool success2 = query.exec("create table LUS_TY_COMMENT(current_date_time varchar primary key,fix_id varchar,comment varchar)");//创建数据表
        if(success2) {
            qDebug() <<"数据库表2创建成功!";
        } else {
            qDebug() <<"数据库表2创建失败!";
            QSqlError tempErr = query.lastError();
            qDebug()<<tempErr;
            return false;
        }

        //创建LUS_TY_LOG
        bool success3 = query.exec("create table LUS_TY_LOG(current_date_time varchar primary key,time varchar ,info varchar)");//创建数据表

        if(success3) {
            qDebug() <<"数据库表3创建成功!";
        } else {
            qDebug() <<"数据库表3创建失败!";
            QSqlError tempErr = query.lastError();
            qDebug()<<tempErr;
        }
    }else{
        open_database();
    }
    return true;
}

QString lus_db::get_time()
{

    QDateTime current_date_time =QDateTime::currentDateTime();
    QString current_date =current_date_time.toString("yyyy.MM.dd hh:mm:ss:z");
    return  current_date;

}

/*处理存储请求
 *根据type，判断出数据类型，用对应结构体指针取出数据，然后存入数据库
 *data： 数据指针
 type：  数据类型
 retun   存储成功返回 true
*/
bool lus_db::lus_store_requst(void  *data, char type)
{
    QSqlQuery query(lus_tydb);
    switch (type){
    case LUS_TY_UPTIME: {                //传感器数据存储
        struct _st_date * date;
        date = (_st_date *)data;

        QString current_date = get_time();
        QString insert_sql = "insert into LUS_UPTIME values (?,?,?,?)";
        query.prepare(insert_sql);

        query.addBindValue(current_date);
        query.addBindValue(date->fix_id);
        query.addBindValue(date->up_date);
        query.addBindValue(date->down_date);

        if(!query.exec())
        {
            qDebug()<<query.lastError();
        } else {
            qDebug()<<"table1 inserted!";
        }
        break;
		}

    case LUS_TY_COMMENT:{
        _st_cm * cm;
        cm = static_cast<_st_cm *>(data);

	QString current_date = get_time();
        QString insert_sql = "insert into LUS_TY_COMMENT values (?,?,?)";
        query.prepare(insert_sql);


        query.addBindValue(current_date);
        query.addBindValue(cm->fix_id);
        query.addBindValue(cm->comment);

        if(!query.exec())
        {
            qDebug()<<query.lastError();
        } else {
            qDebug()<<"table2 inserted!";
        }
        break;}


    case LUS_TY_LOG:   {                     //存储日志数据
        struct _log * log;
        log = static_cast<_log *>(data);

        QString log_time = QString(QLatin1String(log->time));
        QString log_info = QString(QLatin1String(log->info));

	QString current_date = get_time();

        QString insert_sql = "insert into LUS_TY_LOG values (?,?,?)";
        query.prepare(insert_sql);

        query.addBindValue(current_date);
        query.addBindValue(log_time);
        query.addBindValue(log_info);


        if(!query.exec())
        {
            qDebug()<<query.lastError();
        }
        else
        {
            qDebug()<<"table3 inserted!";
        }
        break;}

    default:{
        qDebug() << "no such data type!";
        break;}


    }
    return true;
}

/*处理数据查询请求
 *根据type，判断出数据类型取出数据显示
 *str： 有LUS_SEARCH_NEXT 4,newinfo 5,根据str判断出历史记录或新纪录查询
 type：  数据类型
 return 成功返回true
*/
bool lus_db::lus_search(char type, int cmd)
{

    QSqlQuery query;

    switch (type) {
    case LUS_TY_UPTIME:{
        QString select_sql = "select current_date_time,fix_id,tmp,tmp_de,hum1,adc_v from LUS_UPTIME";
        if(!query.exec(select_sql))
        {
            qDebug()<<query.lastError();
        }
        else
        {
            if(cmd==LUS_SEARCH_NEXT)
            {
                qDebug()<<"LUS_UPTIME的历史记录为：";
                while(query.next())
                {

                    QString fix_id = query.value(1).toString();
                    QString tmp = query.value(2).toString();
                    QString tmp_de = query.value(3).toString();
                    QString hum1 = query.value(4).toString();
                    QString adc_v = query.value(5).toString();
                    qDebug() <<"fix_id:"<< fix_id <<"tmp:"<< tmp << "tmp_de:" << tmp_de << "hum1:" << hum1<<"adc_v:" << adc_v ;
                }
            }
            else
            {
                query.last();

                QString fix_id = query.value(1).toString();
                QString tmp = query.value(2).toString();
                QString tmp_de = query.value(3).toString();
                QString hum1 = query.value(4).toString();
                QString adc_v = query.value(5).toString();
                qDebug()<<"LUS_UPTIME的最新记录为：" <<"fix_id:"<< fix_id <<"tmp:"<< tmp<<"tmp_de:"<<tmp_de<<"hum1:"<<hum1<<"adc_v:"<<adc_v ;

            }

        }
        break;}

    case LUS_TY_COMMENT:{
        QString select_sql = "select current_date_time,fix_id,comment from LUS_TY_COMMENT";
        if(!query.exec(select_sql))
        {
            qDebug()<<query.lastError();
        }
        else
        {
            if(cmd==LUS_SEARCH_NEXT)
            {
                qDebug()<<"LUS_TY_COMMENT的历史记录为：";
                while(query.next())
                {

                    QString fix_id = query.value(1).toString();
                    QString comment = query.value(2).toString();
                    QString z_a = query.value(3).toString();
                    //TODO
                }
            }
            else
            {
                query.last();

                QString x_a = query.value(1).toString();
                QString y_a = query.value(2).toString();
                QString z_a = query.value(3).toString();
                qDebug()<<"LUS_TY_COMMENT的最新记录为：" << "x_a:" << x_a << "y_a:" << y_a << "z_a:" << z_a ;

            }

        }
        break;}

    case LUS_TY_LOG:{
        QString select_sql = "select current_date_time,time,info from LUS_TY_LOG";
        if(!query.exec(select_sql))
        {
            qDebug()<<query.lastError();
        }
        else
        {
            if(cmd==LUS_SEARCH_NEXT)
            {
                qDebug()<<"LUS_TY_LOG的历史记录为:";
                while(query.next())
                {

                    QString time = query.value(1).toString();
                    QString info = query.value(2).toString();
                    qDebug()<< "time:" << time << "info:" << info ;
                }
            }
            else if(cmd == LUS_SEARCH_NEW)
            {
                query.last();

                QString time = query.value(1).toString();
                QString info = query.value(2).toString();
                qDebug()<<"LUS_TY_LOG的最新记录为:" << "time:" << time << "info:" << info ;

            }

        }
        break;}

    default:
    {break;}
    }

    return true;
}




/*查询数据记录总数请求
 *根据type，判断出数据类型，显示数据记录总数
 type：  数据类型
 return 成功返回true
*/
bool lus_db::lus_count(char type)
{
    QSqlQuery query;
    switch (type){

    case LUS_TY_UPTIME: {

        query.exec("SELECT current_date_time FROM LUS_UPTIME");

        //----------获取结果集大小----------//
        if (query.last())

        {

            qDebug()<<"LUS_UPTIME总记录="<<query.at() + 1;

        }

        query.first();//重新定位指针到结果集首位
        query.previous();//如果执行query.next来获取结果集中数据，要将指针移动到首位的上一位。
        break;}

    case LUS_TY_COMMENT:{

        query.exec("SELECT current_date_time FROM LUS_TY_COMMENT");

        //----------获取结果集大小----------//
        if (query.last())

        {

            qDebug()<<"LUS_TY_COMMENT总记录="<<query.at() + 1;

        }

        query.first();//重新定位指针到结果集首位
        query.previous();//如果执行query.next来获取结果集中数据，要将指针移动到首位的上一位。

        break;}

    case LUS_TY_LOG:{

        query.exec("SELECT current_date_time FROM LUS_TY_LOG");

        //----------获取结果集大小----------//
        if (query.last())

        {

            qDebug()<<"LUS_TY_LOG总记录="<<query.at() + 1;

        }

        query.first();//重新定位指针到结果集首位
        query.previous();//如果执行query.next来获取结果集中数据，要将指针移动到首位的上一位。
        break;}

    default:{break;}

    }
    return true;
}



/*处理数据表删除请求
 *根据type，判断出数据类型，删除相应数据表
 type：  数据类型
 return 成功返回true
*/
bool lus_db::lus_deletetable(char type)
{
    QSqlQuery query;
    switch (type) {
    case LUS_TY_UPTIME:{
        query.exec("DROP TABLE LUS_UPTIME");
        break;}
    case LUS_TY_COMMENT:{
        query.exec("DROP TABLE LUS_TY_COMMENT");
        break;}
    case LUS_TY_LOG:{
        query.exec("DROP TABLE LUS_TY_LOG");
        break;}
    default:
    {break;}
    }
   return true;
}
