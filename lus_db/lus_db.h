#ifndef LUS_DB_H
#define LUS_DB_H


#include <QSqlDatabase>

class lus_db
{
public:
        lus_db();

        bool lus_store_requst(void  *data, char type);
        bool lus_database_init(void);
        bool lus_count(char type);
        bool lus_search(char type,int cmd);
        bool lus_deletetable(char type);

private:
        QSqlDatabase lus_tydb;
        bool open_database(void);

        QString get_time();
};

#endif // LUS_DB_H
