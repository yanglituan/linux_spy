﻿#ifndef LUS_DATA_TYPE
#define LUS_DATA_TYPE

#include <stdint.h>
#include <QString>

#define u8	uint8_t
#define u16 uint16_t

#define LUS_DB_LOG_MAX  50

/**/
#define LUS_SEARCH_NEXT 1
#define LUS_SEARCH_NEW  2




#define LUS_TY_UPTIME  1
struct _st_date{

    u8 data_type;
    QString fix_id;
    QString up_date;
    QString down_date;

};

#define LUS_TY_COMMENT	2
struct _st_cm{


    u8 data_type;
	QString fix_id;
	QString comment;

};

#define LUS_TY_LOG  3
struct _log{
	
    u8 data_type;
	char time[20];
    char info[LUS_DB_LOG_MAX];

};

#define historyinfo  4
#define newinfo      5

#endif 
