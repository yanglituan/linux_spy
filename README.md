
# linux SPY

## 简介

使用Qt开发的linux嵌入式设备监控、管理框架

[客户端仓库](https://gitee.com/zhaojun_chao/aes_tcp_lib)
## 开发环境
* ubuntu 20
* Qt 5.12
* Qt Creator 4.13.1

## 核心功能
* 监测嵌入式设备运行状态
* 转发客户端消息, 为客户端提供消息转发服务
* 对设备执行远程shell
* 对设备进行远程升级，管理设备固件版本
* 与客户端进行文件传输

![主界面图](./lus/linux_spy_main.png)
![设备上线](./lus/lus.png)
![设备树形](./lus/lus-2.png)

# 第三方依赖

- https://gitee.com/ldcsaa/HP-Socket
- https://github.com/MEONMedical/Log4Qt

第三方依赖的动态库以及编译了一份[点我下载](https://pan.baidu.com/s/1HdaRydZcpAbbvAAVseJxUw) 提取码: cf32
下载到工程文件夹解压，即可编译本项目，解压后的lib目录结构如下
```
> $ tree ./lib
lib
├── libhpsocket.a
├── libhpsocket_d.a
├── libhpsocket_d.so
├── libhpsocket_d.so.5 -> libhpsocket_d.so
├── libhpsocket_d.so.5.7.2 -> libhpsocket_d.so
├── libhpsocket_d.so.5.7.3 -> libhpsocket_d.so
├── libhpsocket.so
├── libhpsocket.so.5 -> libhpsocket.so
├── libhpsocket.so.5.7.2 -> libhpsocket.so
├── libhpsocket.so.5.7.3 -> libhpsocket.so
├── liblog4qt.so -> liblog4qt.so.1.0.0
├── liblog4qt.so.1 -> liblog4qt.so.1.0.0
├── liblog4qt.so.1.0 -> liblog4qt.so.1.0.0
└── liblog4qt.so.1.0.0

0 directories, 14 files

```
