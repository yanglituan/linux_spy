#ifndef DIALOG_COMMENT_H
#define DIALOG_COMMENT_H

#include <QDialog>

namespace Ui {
class Dialog_comment;
}

class Dialog_comment : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog_comment(QWidget *parent = nullptr);
    ~Dialog_comment();

    QString comment;

private slots:
    void on_pushButton_ok_clicked();

private:
    Ui::Dialog_comment *ui;
};

#endif // DIALOG_COMMENT_H
