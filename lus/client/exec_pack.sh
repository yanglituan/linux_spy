#!/bin/bash

#检测目录是否存在
if test -d ./lus/exec
then 
		echo "find exec dir"
else 
		mkdir ./lus/exec -p
		exit
fi 


#检测是否输入了参数
if test -z $1
then
		echo "no command"
		exit
fi

mv $1 ./lus/down

cd ./lus/exec/

dir_full_name=$1

dir_name=${dir_full_name%%.*}

echo $dir_name


if test -d $dir_name 
then
	echo "rm dir"
	rm -rf $dir_name
fi

mkdir  $dir_name
	
cp  ../down/$1 $dir_name 
cd $dir_name
unzip $1

rm $1 -f

if test -e exec.sh
then
		chmod +x exec.sh 
		./exec.sh
else
		echo "not found exec_pack.sh"
fi








