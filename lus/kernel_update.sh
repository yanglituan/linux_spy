#!/bin/sh

#参数说明
#	$1 kernel 所在的分区
# 	$2 新内核路径


#构建目录
if test -d /lus/kernel_update_d
then
	echo "dir is ok"
else 
	mkdir /lus/kernel_update_d -p
fi

if test -d /lus/log
then
	echo "dir is ok"
else 
	mkdir /lus/log -p
fi

#挂载内核分区

if test -e /dev/$1
then
	mount  /dev/$1 /lus/kernel_update_d
else
	date >> /lus/log/error_log
	echo "no such node file" >>  /lus/log/error_log
	echo " " >>  /lus/log/error_log
	
	exit
fi

#更新内核
if test -e $2
then
	chmod 777 $2
	cp $2 /lus/kernel_update_d
	umount	/lus/kernel_update_d
	
fi



