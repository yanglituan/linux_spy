#ifndef BAIDU_MAP_H
#define BAIDU_MAP_H

#include <QJsonDocument>
#include <QJsonParseError>
#include <QFile>
#include <QJsonObject>
#include <QDebug>
#include <QJsonArray>
#include <QtNetwork>
#include <QObject>
struct post_ip_info
{
    QString ip;
    QString fix_id;
    QString addr;
};

class baidu_map :public QObject
{
    Q_OBJECT

public:

    void ip_to_addr(post_ip_info info);
    void baidu_map_init(void);
    post_ip_info post_ack;
private slots:
    void post_slot();
    void replyFinished(QNetworkReply *reply);
private:
    void prase_ip_info(QByteArray data);
    void post_ip(QString ip);
    QTimer *post_timer;
    QList <post_ip_info> post_list;
    QNetworkAccessManager *manager;
    QNetworkRequest *request = new QNetworkRequest();

signals:
    void ip_to_addr_sig();

};

#endif // BAIDU_MAP_H
