#ifndef LUS_ST_H
#define LUS_ST_H

#include <QString>
#include <QList>
#include <QFile>
#include <QCryptographicHash>
#include <qdebug.h>

/*discribe licence*/
struct m_licence{
    QString licence_str;
    QByteArray key;

};

/*client  struct*/
    struct transfer_inf{

        QFile * localFile;
        qint64 totalBytes;  //数据总大小
        qint64 bytesWritten;  //已经发送数据大小
        qint64 bytesToWrite;   //剩余数据大小

    };

    struct _tcp_msg_inf{

        quint32 blocksize;
        quint32 load_size;
        unsigned char  *cache;
        quint8 encrypt_flag;
    };
    struct _down_info{

        int status;     //是否执行下载
        qint64 type;       //下载类型
        QString file_path;
    };

    struct _bd_info{

        QString cpu_info,
                hd_info,
                cpu_temp;
                //client_id;
    };

    /* discribe client*/
    struct m_client{

        QString fix_id;		//写到clinet代码里的id，用于识别
        quint32 var_id;		//服务器临时分配，通讯时使用
        QString group_id;
        QString clinet_ip;
        QString ip2place;
        QString licence;
        QString comment;
        QDateTime up_time;
        QDateTime down_time;
        uint16_t heart;		//心跳变量
        uint64_t msg_count;
        QString firmware_ver;
        bool checked;
        struct _bd_info bd_info;
        struct transfer_inf file_inf;
        struct _down_info down_info;
        struct	_tcp_msg_inf tcp_msg_inf;
        void *socket;
        void *rsa;
    };

    typedef  struct m_client  lus_client_st;
    typedef  struct m_client*  lus_client_stp;
    typedef QList<struct m_client *> lus_client_list;
    typedef QList<struct m_client *>* lus_client_listp;
/*******配置宏********/

/*初始client heart时间*/
#define DEF_HEART_TIME  160
#define DEF_FILE_CLIENT_HEART_TIME  60
#define HEART_CHECK		1
#define TO_CLIENT_HEART  1


/*******************/

#define ENCRYPT_YES 0x12
#define ENCRYPT_NO 	0x7

    enum DOWN_TYPE{

        ST_PACK = 0,
        ST_FILE
    };

    enum DOWN_STATUS{
        DOWN_ING = 0,
        DOWN_OK,
        DOWN_WAITE,
        DOWN_ERROR,
        DOWN_DISABLE
    };
    class lus_log
    {
    public:
        enum LOG_LEVE{
            LG_INFO = 0,
            LG_WARING,
            LG_DEBUG,
            LG_ERROR
        };
        QString get_file_md5(QString str_path)
        {

            QFile theFile(str_path);
            theFile.open(QIODevice::ReadOnly);
            QByteArray ba = QCryptographicHash::hash(theFile.readAll(), QCryptographicHash::Md5);
            theFile.close();
            return ba.toHex().constData();
        }

        void log_printf(QString log, int level=1)
        {

            qDebug()<<log;
        }

    };
#endif // LUS_ST_H

