#include "terminal_cmd.h"
#include "ui_terminal_cmd.h"
#include <QScrollBar>

terminal_cmd::terminal_cmd(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::terminal_cmd)
{
    ui->setupUi(this);

    ui->lineEdit_cmd->installEventFilter(this);//设置完后自动调用其eventFilter函数

    ui->lineEdit_cmd->setFocus();
    cmd_history.append("head");
}

terminal_cmd::~terminal_cmd()
{
    delete ui;
}


/*显示terminal 返回信息*/
void terminal_cmd::get_terminal_bak(QString str)
{

    ui->textEdit->append(str);
    QScrollBar *bar = ui->textEdit->verticalScrollBar();
    bar->setValue(bar->maximum());
}


bool terminal_cmd::eventFilter(QObject *target, QEvent *event)
{
    if(target == ui->lineEdit_cmd)
    {
        if(event->type() == QEvent::KeyPress)//回车键
        {
            QKeyEvent *k = static_cast<QKeyEvent *>(event);
            if(k->key() == Qt::Key_Return)
            {
                if(ui->lineEdit_cmd->text() == "clr"){
                    ui->textEdit->clear();
                    return true;
                }
                mix_xml(ui->lineEdit_cmd->text());
                //qDebug()<<"last :"<<cmd_history.last();

                if(ui->lineEdit_cmd->text() != cmd_history.last()){
                    cmd_history.append(ui->lineEdit_cmd->text());
                    if(cmd_history.size()>CMD_MAX){
                        cmd_history.removeFirst();
                    }
                    cmd_index = cmd_history.size()-2;
                }

                if(ui->checkBox_auto_clr->isChecked()){
                    ui->lineEdit_cmd->clear();
                }
                //qDebug() << "enter key";
                return true;
            }else if(k->key() == Qt::Key_Up){
                 ui->lineEdit_cmd->setText(cmd_history.at(cmd_index));
                 if(cmd_index >=1 )
                    cmd_index -= 1;
                 return true;
             }else if(k->key() == Qt::Key_Down){

                 ui->lineEdit_cmd->setText(cmd_history.at(cmd_index));
                 if(cmd_index < cmd_history.size()-1)
                    cmd_index += 1;
                 return true;
             }
        }
    }
    return QWidget::eventFilter(target,event);
}


void terminal_cmd::mix_xml(QString str_cmd)
{
    QString str = "<lus_term_cmd";

    str += (" body=\"" + str_cmd + "\"");
    str += "/>";

    emit terminal_cmd_ready(str);
}

